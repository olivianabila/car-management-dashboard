# Car Management Dashboard
Car Management Dashboard is a system that allows admin user to add, edit and delete some cars.
Technologies used: Express, Node.js, PostgreSQL, view engine ejs

## How to run
guide 

```bash
nodemon index.js 
```

## Endpoints
GET("/api/v1/cars") 

GET("/api/v1/cars/:id") 

POST("/api/v1/cars") 

POST("/api/v1/cars-upload") 

PUT("/api/v1/cars/:id") 

DELETE("/api/v1/cars/:id")



## ERD
![Entity Relationship Diagram](public/assets/img/erd.png)

## tampilan
![tampilan](public/assets/img/all.png)

![small](public/assets/img/small.png)

![medium](public/assets/img/medium.png)

![large](public/assets/img/large.png)








